ARG TERRAFORM_VERSION=0.14.7
ARG TERRAGRUNT_VERSION=v0.23.31
FROM node:12.19.0-alpine3.12 as node

RUN chown -R root:root /usr
RUN apk --update add              \
      curl                        \
      bash                        \
      grep                        \
      git                         \
      jq                          \
      openssh                     \
      unzip                       \
      zip                         \
      py3-pip                     \
      docker-cli                  \
      docker-compose              \
      alpine-sdk                  \
      libxml2-dev                 \
      libxml2                     \
      libxslt-dev                 \
      gcc                         \
      python3-dev                 \
      musl-dev                    \
      && \
    pip3 install --upgrade setuptools pip && \
    pip3 install --upgrade awscli && \
    pip3 install --upgrade terraform-compliance && \
    rm -rf /var/cache/apk/* && \
    update-ca-certificates

# Install Terraform
ARG TERRAFORM_VERSION
RUN curl https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip > terraform_${TERRAFORM_VERSION}_linux_amd64.zip && \
    unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip -d /bin && \
    rm -f terraform_${TERRAFORM_VERSION}_linux_amd64.zip

# Install Terragrunt
ARG TERRAGRUNT_VERSION
RUN wget https://github.com/gruntwork-io/terragrunt/releases/download/${TERRAGRUNT_VERSION}/terragrunt_linux_amd64 && \
    chmod 755 terragrunt_linux_amd64 && \
    mv terragrunt_linux_amd64 /usr/local/bin/terragrunt

RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
                unzip awscliv2.zip && \
                ./aws/install && \
                aws --version
