# ========================================================
# VARIABLES
# ========================================================
TEKTON_CHART=https://storage.googleapis.com/tekton-releases/pipeline/latest/release.notags.yaml
TEKTON_TRIGGER_CHART=https://storage.googleapis.com/tekton-releases/triggers/latest/release.yaml
TEKTON_DASHBOARD_CHART=https://github.com/tektoncd/dashboard/releases/download/v0.1.1/release.yaml

# mark jobs as phony so they don't run unless specified.
.PHONY : setup-mac start info build apply

main : 
	@ echo "| command   | description                        |"
	@ echo "| --------- | ---------------------------------- |"
	@ echo "| help      | Prints list of commands            |"
	@ echo "| setup-mac | Prepare your mac for development   |"
	@ echo "| start     | Start minikube and install Tekton  |"
	@ echo "| info      | Show info regarding Tekton         |"
	@ echo "| build     | Build docker images for your tasks |"
	@ echo "| apply     | Apply Tekton configuration         |"

help : main

# ========================================================
# SETUP-MAC - Install dependencies required for dev on OSX
# ========================================================
setup-mac :
	brew install minikube tektoncd-cli

# ========================================================
# START - Minikibe and install our Tektok dependencies
# ========================================================
start :
	minikube start
	kubectl apply --filename ${TEKTON_CHART}
	kubectl apply --filename ${TEKTON_TRIGGER_CHART}
	kubectl apply --filename ${TEKTON_DASHBOARD_CHART}

# ========================================================
# INFO - Reports information about Tekton deployment
# ========================================================
info :
	# ====================================================
	# PODS
	# ====================================================
	kubectl get pods -n tekton-pipelines
	# ====================================================
	# RESOURCES
	# ====================================================
	kubectl api-resources --api-group='tekton.dev'

# ========================================================
# BUILD - Build our container images ready to be used by 
# our piplines and tasks
# ========================================================
build :
	(cd wrapper && make build)
	docker build -t self-service:latest .

# ========================================================
# APPLY - Deploys our Tekton resource to the cluster
# ========================================================
apply :
	# ====================================================
	# TASK - Terraform
	# ====================================================
	kubectl apply -f takton/task-terraform.yml
	# ====================================================
	# PIPELINE - Terraform Label
	# ====================================================
	kubectl apply -f tekton/pipeline-terraform-label.yml


# ========================================================
# TEST - Test everything
# ========================================================
test : 
	(cd wrapper && make test)