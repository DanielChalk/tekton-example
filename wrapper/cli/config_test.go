package cli

import (
	"github.com/kataras/golog"
	"os"
	"testing"
)

func TestIncorrectActionErrors(t *testing.T) {
	golog.SetLevel("debug")
	os.Clearenv()
	os.Setenv(VarTerraformAction, "not_an_action")
	_, err := Setup()

	if err == nil {
		t.Errorf("expected error nil given")
	}
	os.Clearenv()
}

func TestSetupFailsEarly(t *testing.T) {
	os.Clearenv()
	os.Setenv(VarDry, "NOT_TRUTHY")
	os.Setenv(VarTerraformAction, "plan")
	_, err := Setup()

	if err == nil {
		t.Errorf("expected error, nil given")
	}

	os.Clearenv()
}
