package cli

import (
	"encoding/json"
	"fmt"
	"strings"

	"adaptavist.com/tw/terraform"
	"adaptavist.com/tw/utils"
	"github.com/kataras/golog"
)

const (
	// VarDry - Is the run going to be dry?
	VarDry = "DRY"
	// VarBackendRegion - Backend S3 Bucket region
	VarBackendRegion = "BACKEND_REGION"
	// VarBackendBucket - Backend S3 Bucket name
	VarBackendBucket = "BUCKET"
	// VarBackendTable - Backend Lock Table name
	VarBackendTable = "TABLE"
	// VarBackendEncrypt - Backend is encrypted
	VarBackendEncrypt = "ENCRYPT"
	// VarAwsAccountID = "ACCOUNT_ID"
	VarAwsAccountID = "AWS_ACCOUNT_ID"
	// VarIamRole - Role Terraform should assume
	VarIamRole = "AWS_IAM_ROLE"
	// VarRequireAwsAccountID - Is VarAwsAccountID required
	VarRequireAwsAccountID = "REQUIRE_AWS_ACCOUNT_ID"
	// VarTerraformVars - JSON Map of terraform variables
	VarTerraformVars = "VARS"
	// VarTerraformArgs - JSON list of args we want to manually apply
	VarTerraformArgs = "ARGS"
	// VarTerraformAction - Which action do you want Terraform to take?
	VarTerraformAction = "ACTION"
)

func defaultIamRoles() []string {
	return []string{"OrganizationAccountAccessRole", "AWSControlTowerExecution"}
}

// SetupBackend - Creates Terraform backend
func SetupBackend() (terraform.BackendConfig, error) {
	missing := []string{}
	backend := terraform.BackendConfig{}

	BackendRegion, ok := GetEnv(VarBackendRegion)

	if ok {
		missing = append(missing, VarBackendRegion)
	} else {
		backend.Region = BackendRegion
	}

	BackendBucket, ok := GetEnv(VarBackendBucket)

	if ok {
		missing = append(missing, VarBackendBucket)
	} else {
		backend.Bucket = BackendBucket
	}

	BackendTable, ok := GetEnv(VarBackendTable)

	if ok {
		missing = append(missing, VarBackendTable)
	} else {
		backend.Table = BackendTable
	}

	BackendEncrypt, error := GetEnvBool(VarBackendEncrypt)

	if error == nil {
		missing = append(missing, fmt.Sprintf("Missing %s", VarBackendEncrypt))
	} else {
		backend.Encrypt = BackendEncrypt
	}

	if len(missing) > 0 {
		return backend, fmt.Errorf("%s missing", strings.Join(missing, ","))
	}

	return backend, nil
}

// SetupVariables - sets up variables for a Terraform run
func SetupVariables() (terraform.Variables, error) {
	variables := terraform.Variables{}
	variablesStr, ok := GetEnv(VarTerraformVars)

	if ok {
		err := json.Unmarshal([]byte(variablesStr), &variables)
		if err == nil {
			return variables, nil
		}
		return variables, err
	}

	return variables, nil
}

// SetupArguments - Sets up argument for a Terraform run
func SetupArguments() ([]string, error) {
	args := []string{}
	argsStr, ok := GetEnv(VarTerraformArgs)

	if ok {
		err := json.Unmarshal([]byte(argsStr), &args)
		if err == nil {
			return args, nil
		}
		return args, err
	}

	return args, nil
}

// SetupAction - Set up Terraform action to execute
func SetupAction() (string, error) {
	if action, ok := GetEnv(VarTerraformAction); ok {
		for _, a := range terraform.Actions() {
			if a == action {
				return action, nil
			}
		}
		return action,
			fmt.Errorf("unknown action %s. Expected %s",
				action,
				strings.Join(terraform.Actions(), ", "))
	}

	return "", fmt.Errorf("%s is not set", VarTerraformAction)
}

// Setup - Creates a Terraform run based on the environment
func Setup() (terraform.Terraform, error) {
	// Even with errors we will set values on tf for debugging
	tf := terraform.Terraform{}

	logger := golog.Child("config")

	// Backend
	backend, backendErr := SetupBackend()
	tf.Backend = backend
	if backendErr != nil {
		return tf, backendErr
	}

	logger.Info("prepared backend")
	logger.Debug(utils.EasyMarshal(tf.Backend))

	// Variables
	variables, variablesErr := SetupVariables()
	tf.Variables = variables
	if variablesErr != nil {
		return tf, variablesErr
	}

	logger.Info("prepared variables")
	logger.Debug(utils.EasyMarshal(tf.Variables))

	// Arguments
	args, argsErr := SetupArguments()
	tf.Arguments = args
	if argsErr != nil {
		return tf, argsErr
	}

	logger.Info("prepared arguments")
	logger.Debug(utils.EasyMarshal(tf.Arguments))

	// Dry run or not?
	if EnvIsSet(VarDry) {
		dry, err := GetEnvBool(VarDry)
		tf.Dry = dry
		if err != nil {
			return tf, err
		}
	} else {
		tf.Dry = false
	}

	if tf.Dry {
		logger.Info("dry run enabled")
	}

	// Teraform action
	action, err := SetupAction()
	tf.Action = action
	if err != nil {
		return tf, err
	}

	// AWS Stuff
	// RequireAwsAccountID, err = GetEnvBool(VarRequireAwsAccountID)
	// AwsAccountID, AwsAccountIDErr := GetEnv(VarAwsAccountID)
	// TODO: AWS Sessions!

	return tf, nil
}
