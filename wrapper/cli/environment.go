package cli

import (
	"errors"
	"os"
	"strconv"
)

// GetEnv - Look up env var - only exists to make this API consistant.
func GetEnv(key string) (string, bool) {
	return os.LookupEnv(key)
}

// GetEnvOr - Returns env value or a fallback value if it doesn't exist.
func GetEnvOr(key string, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

// GetEnvBool - Get env value and parses it to bool
func GetEnvBool(key string) (bool, error) {
	strValue, ok := GetEnv(key)
	if ok {
		boolValue, err := strconv.ParseBool(strValue)
		if err != nil {
			return boolValue, err
		}
		return boolValue, nil
	}
	return false, errors.New("Failed to find env var")
}

// GetEnvBoolOr - Returns an env value or a fallback value as a bool
func GetEnvBoolOr(key string, fallback bool) bool {
	if value, err := strconv.ParseBool(GetEnvOr(key, "false")); err == nil {
		return value
	}

	return false
}

// EnvIsSet - Is the env var set?
func EnvIsSet(key string) bool {
	if _, ok := os.LookupEnv(key); ok {
		return true
	}

	return false
}
