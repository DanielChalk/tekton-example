package terraform

import (
	"fmt"
	"os"
	"strconv"

	"github.com/kataras/golog"
)

// BackendConfig - Configuration of a Terraform Backend
type BackendConfig struct {
	Region  string
	Bucket  string
	Table   string
	Encrypt bool
	Key     string
}

const backendFileName = "_backend.tf"
const backendTemplate = `terraform {
	backend "s3" {
		region         = "%s"
		bucket         = "%s"
		dynamodb_table = "%s"
		encrypt        = %s
		key            = "%s"
	}
}
`

func check(e error) {
	if e != nil {
		panic(e)
	}
}

// Generate - Generate backend terraform configuration
func (config BackendConfig) generate() string {
	return fmt.Sprintf(backendTemplate,
		config.Region,
		config.Bucket,
		config.Table,
		strconv.FormatBool(config.Encrypt),
		config.Key)
}

// Create - Create backend terraform config file
func (config BackendConfig) Create(dry bool) (string, error) {
	logger := golog.Child("backend")
	configStr := config.generate()
	logger.Debugf("backend config\n%s", configStr)

	if !dry {
		file, err := os.Create(backendFileName)

		if err != nil {
			return "", err
		}

		file.WriteString(configStr)
		file.Sync()
		file.Close()
	}

	logger.Debugf("Generated %s\n%s\n", backendFileName, configStr)

	return configStr, nil
}
