package terraform

import (
	"testing"
)

func TestBackendConfig(t *testing.T) {
	backend := BackendConfig{
		Region:  "us-east-1",
		Bucket:  "bucket",
		Table:   "table",
		Encrypt: true,
		Key:     "meh",
	}
	config, err := backend.Create(true)

	if err != nil {
		t.Error("error expected to be nil, got: ", err.Error())
	}

	expected := `terraform {
	backend "s3" {
		region         = "us-east-1"
		bucket         = "bucket"
		dynamodb_table = "table"
		encrypt        = true
		key            = "meh"
	}
}
`
	if config != expected {
		t.Error("should match", config, expected)
	}
}
