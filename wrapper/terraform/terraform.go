package terraform

import (
	"os/exec"
	"strings"

	"github.com/kataras/golog"
)

const (
	plan    = "plan"
	apply   = "apply"
	destroy = "destroy"
	refresh = "refresh"
)

// Actions - Returns list of possible terraform actions
func Actions() []string {
	return []string{plan, apply, destroy, refresh}
}

// Terraform - Represents a operation on a Terraform stack/workspace
type Terraform struct {
	Dry bool
	// action - the Terraform action to perform
	Action string
	// arguments list of arguments to append to the commant
	Arguments []string
	// variables - used in plans/apply operations
	Variables Variables
	// backend - configuration for where Terraform stores its state
	Backend BackendConfig
}

func logger() *golog.Logger {
	return golog.Child("terraform")
}

// execOrPrint - Prints the command and executes it if not dry
func execOrPrint(dry bool, arguments []string) error {
	logger().Info(strings.Join(append([]string{"terraform"}, arguments...), " "))

	if !dry {
		// We want stdin and stderr hence the combined output
		result, err := exec.Command("terraform", arguments...).CombinedOutput()
		logger().Info(result)
		if err != nil {
			return err
		}
	}

	return nil
}

// TerraformArguments - Build arguments for the terraform binary
func (tf Terraform) TerraformArguments(action string) []string {
	arguments := []string{action}

	// only append argumnt if they are provided
	if tf.Arguments != nil {
		arguments = append(arguments, tf.Arguments...)
	}

	return arguments
}

// terraformInit - initialise the backend
func (tf Terraform) terraformInit() error {
	err := execOrPrint(tf.Dry, []string{"init", "-input=false"})

	if err != nil {
		return err
	}

	return nil
}

// TerraformRun - runs a terraform operation
func (tf Terraform) TerraformRun() error {
	_, err := tf.Backend.Create(tf.Dry)

	if err != nil {
		return err
	}

	err = tf.terraformInit()

	if err != nil {
		return err
	}

	err = tf.Variables.Create(tf.Dry)

	if err != nil {
		return err
	}

	arguments := tf.TerraformArguments(tf.Action)
	err = execOrPrint(tf.Dry, arguments)

	return nil
}
