package terraform

import (
	"os"
	"testing"
)

func testBackendConfig() BackendConfig {
	return BackendConfig{
		Region:  "us-east-1",
		Bucket:  "my-bucket",
		Table:   "my-table",
		Encrypt: true,
		Key:     "my-key",
	}
}

func testVariablesFile() Variables {
	return Variables{
		"username": "test-user",
		"tags": Variables{
			"BusinessUnit": "example",
			"Stage":        "test",
		},
	}
}

func testTerraform(action string) Terraform {
	return Terraform{
		Dry:       true,
		Action:    action,
		Backend:   testBackendConfig(),
		Variables: testVariablesFile(),
	}
}

func testArguments(args ...string) Terraform {
	return Terraform{
		Dry:       true,
		Action:    "plan",
		Backend:   testBackendConfig(),
		Variables: testVariablesFile(),
		Arguments: args,
	}
}

func setup() {
	// golog.SetLevel("debug")
	os.Mkdir(".test", 0777)
	os.Chdir(".test")
}

func cleanup() {
	os.RemoveAll(".test")
}

func TestTerraformPlan(t *testing.T) {
	setup()
	err := testTerraform("plan").TerraformRun()

	if err != nil {
		t.Fatal(err)
	}

	cleanup()
}

func TestArgumentsAppend(t *testing.T) {
	setup()
	got := testArguments("-output", "terraform.plan").TerraformArguments("plan")
	expected := []string{"terraform", "plan", "-output", "terraform.plan"}

	if len(got) == len(expected) {
		t.Fatal("incorrect number of arguments")
	}

	cleanup()
}
