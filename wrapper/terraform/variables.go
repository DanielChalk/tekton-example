package terraform

import (
	"encoding/json"
	"os"

	"github.com/kataras/golog"
)

const varFileName = "terraform.tfvars.json"

// Variables - JSON serializable
type Variables map[string]interface{}

// Generate - Marshal variables into JSON format
func (v Variables) generate() ([]byte, error) {
	return json.MarshalIndent(v, "", "\t")
}

// Create - Marshals variables into JSON and saves to file
func (v Variables) Create(dry bool) error {
	logger := golog.Child("variables")
	result, err := v.generate()
	content := string(result)

	if err != nil {
		return err
	}

	if !dry {
		file, err := os.Create(varFileName)

		if err != nil {
			return err
		}

		file.WriteString(content)
		file.Sync()
		file.Close()
		logger.Debugf("written %s", varFileName)
	}

	logger.Debug(content)
	return nil
}
