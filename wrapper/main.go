package main

import (
	"adaptavist.com/tw/cli"
)

func main() {
	tf, err := cli.Setup()

	if err != nil {
		panic(err)
	}

	err = tf.TerraformRun()

	if err != nil {
		panic(err)
	}

}
