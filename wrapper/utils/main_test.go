package utils

import "testing"

func TestEasyMarshalErrors(t *testing.T) {
	str := EasyMarshal(make(chan int))

	if str != "" {
		t.Errorf("expected empty string, go %s", str)
	}
}
