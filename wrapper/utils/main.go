package utils

import "encoding/json"

// EasyMarshal returns an empty string if an error occurs, used for logging purposes only.
func EasyMarshal(v interface{}) string {
	bytes, err := json.Marshal(v)

	if err != nil {
		return ""
	}

	return string(bytes)
}
